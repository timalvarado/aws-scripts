# Get DNS Records

## Description

This script will pull given DNS record types for a given zone.

### Prerequisites

* Python
* Boto3

### How-To

The script takes multiple arguments. I will show some examples below

This will return records for a given public zone
```
python get_dns_records.py --zone myzone.com --recordtype A,CNAME
```

This will return records for a given private zone
```
python get_dns_records.py --zone myzone.com --recordtype A,CNAME --private
```
