# AWS Scripts

## Description

This repo hosts aws scripts I wrote to make my life a little easier. 

### Prerequisites

* Python

 ## Authors

* **Tim Alvardo** - *Developer/Maintainer* - [Timalvarado](https://gitlab.com/timalvarado)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
