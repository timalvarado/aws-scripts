import boto3
import argparse


def getZoneID(zone, private, client):
    """
    getZoneID takes in the zone name and if the zone is a private one. It will output the zoneID for that given Route53 hosted zone(private or not)
    """
    zone = zone + '.'
    response = client.list_hosted_zones()
    for dns_zone in response['HostedZones']:
        if dns_zone['Name'] == zone and dns_zone['Config']['PrivateZone'] == private:
            return dns_zone['Id'].split('/')[2]

def getDNSRecords(zoneID, client):
    """
    getDNSRecords takes in zoneID. It will return all of the records of a given type
    """
    try:
        response = client.list_resource_record_sets(
            HostedZoneId=zoneID
        )
        return response
    except:
        print("Make sure you entered a valid recordtype")


def main():
    parser = argparse.ArgumentParser(
        description='Pull resouce records from Route53'
    )
    parser.add_argument(
        '-z',
        '--zone',
        required=True,
        type=str,
        help='Zone Name. Do not worry about the trailing dot.'
    )
    parser.add_argument(
        '-rt',
        '--recordtype',
        required=False,
        help='Record Types. Can give a list like so A,CNAME,MX',
        type=lambda s: [str(item) for item in s.split(',')]
    )
    parser.add_argument(
        '-pz',
        '--private',
        help='Private Zone or not. By default it assumes the zone is public.',
        default=False,
        action=argparse.BooleanOptionalAction
    )

    args = parser.parse_args()
    client = boto3.client('route53')
    zoneID = getZoneID(args.zone, args.private, client)
    dns_records = getDNSRecords(zoneID, client)
    for dns_record in dns_records['ResourceRecordSets']:
        if dns_record['Type'] in args.recordtype:
            dns_name = dns_record['Name']
            dns_recordtype = dns_record['Type']
            for dns_value in dns_record['ResourceRecords']:
                dns_value = dns_value['Value']
            output = (
                f'{dns_name:<40} {dns_recordtype} {dns_value}'
            )
            print(output)


if __name__ == "__main__":
    main()
